(defproject fizzbuzz "0.1.0-SNAPSHOT"
  :description "FIXME"
  :url "NONE"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [compojure "1.1.1"]
                 [org.clojure/data.json "0.2.0"]
                 [ring/ring-jetty-adapter "1.1.0"]]
  :min-lein-version "2.0.0"
  :plugins [[lein-ring "0.7.1"]]
  :ring {:handler fizzbuzz.routes/app})

