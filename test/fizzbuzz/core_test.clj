(ns fizzbuzz.core-test
  (:use clojure.test
        fizzbuzz.core))

(deftest fb-test
  (testing "FizzBuzz"
    (let [results (fizz-buzz 1 20)]
      (is (= 20 (count results)))
      (is (= "Fizz" (nth results 2)))
      (is (= "Buzz" (nth results 4)))
      (is (= "FizzBuzz" (nth results 14)))
      ))
  (testing "empty input"
    (let [results (fizz-buzz 10 1)]
      (is (= 0 (count results))))))
