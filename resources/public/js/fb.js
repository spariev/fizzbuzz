function preparePostData(startVal, endVal){
  return "start=" + startVal + "&stop=" + endVal ;
}
function prepareUri(startVal, endVal){
  return "/fizz-buzz.json" + "?" + preparePostData(startVal, endVal);
}

function renderTable(data, startVal, endVal) {
    $("#results-table").html('');
    $("#results-table").append("<tr><th>Displaying FizzBuzz results for range " + startVal + " to " + endVal + "</th></tr>");
    $.each(data, function(i,item) {
        $("#results-table").append("<tr><td>" + item + "</td></tr>");
    });
};
