(ns fizzbuzz.heroku
  (:require [fizzbuzz.routes :as routes])
  (:use [ring.adapter.jetty :only [run-jetty]]))

(defn -main [port]
  (run-jetty routes/app {:port (Integer. port)}))
