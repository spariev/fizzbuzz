(ns fizzbuzz.core)


(defn fizz-buzz*
  "Classic FizzBuzz function"
  [num]
  (cond
   (and (= 0 (mod num 3)) (= 0 (mod num 5))) "FizzBuzz"
   (= 0 (mod num 3)) "Fizz"
   (= 0 (mod num 5)) "Buzz"
   :else num))

(defn fizz-buzz
  "Classic FizzBuzz function - entry point"
  [from to]
  (map fizz-buzz* (range from (inc to))))
