(ns fizzbuzz.views
  (:require [clojure.data.json :as json]
            [fizzbuzz.core :as core]))

(defn parse-int
  [num]
  (try
    (Integer/parseInt num)
    (catch Exception ex 0)))

(defn fizz-buzz
  [start end]
  (let [results (core/fizz-buzz (parse-int start) (parse-int end))]
    {:status 200
     :headers {"Content-Type" "application/json"}
     :body     (json/write-str results)}))
