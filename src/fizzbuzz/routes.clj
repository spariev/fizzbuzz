(ns fizzbuzz.routes
  (:use compojure.core)
  (:require [compojure.route :as route]
            [compojure.handler :as handler]
            [compojure.response :as response]
            [ring.util.response :as resp]
            [fizzbuzz.views :as views]))

(defroutes main-routes
  (POST "/fizz-buzz.json" [start stop] (views/fizz-buzz start stop))
  (GET "/" [] (resp/file-response "index.html" {:root "resources/public"}))
  (route/resources "/")
  (route/not-found "Page not found"))

(def app
  (-> (handler/api main-routes)))
